﻿using AffiliateMS.Domain.Entities;
using AffiliateMS.Messaging.Send.Options;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Text;

namespace AffiliateMS.Messaging.Send.Sender
{
    public class AffiliateUpdateSender : IAffiliateUpdateSender
    {
        private readonly string hostname;
        private readonly string queueName;
        private readonly string username;
        private readonly string password;

        public AffiliateUpdateSender(IOptions<RabbitMqConfiguration> rabbitMqOptions)
        {
            hostname = rabbitMqOptions.Value.Hostname;
            queueName = rabbitMqOptions.Value.QueueName;
            username = rabbitMqOptions.Value.UserName;
            password = rabbitMqOptions.Value.Password;
        }

        public void SendAffiliate(Affiliate affiliate)
        {
            var factory = new ConnectionFactory() { HostName = hostname, UserName = username, Password = password };
            try
            {
                using (var connection = factory.CreateConnection())
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDeclare(queue: queueName, durable: false, exclusive: false, autoDelete: false, arguments: null);

                    var json = JsonConvert.SerializeObject(affiliate);
                    var body = Encoding.UTF8.GetBytes(json);

                    channel.BasicPublish(exchange: "", routingKey: queueName, basicProperties: null, body: body);
                }
            } catch (Exception e)
            {
                throw new Exception($"Something went wrong: {e.Message}");
            }
        }
    }
}
