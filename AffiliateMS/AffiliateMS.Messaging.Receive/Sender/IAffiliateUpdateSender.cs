﻿using AffiliateMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace AffiliateMS.Messaging.Send.Sender
{
    public interface IAffiliateUpdateSender
    {
        void SendAffiliate(Affiliate affiliate);
    }
}
