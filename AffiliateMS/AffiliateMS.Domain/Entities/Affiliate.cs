﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AffiliateMS.Domain.Entities
{
    public class Affiliate
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Identification { get; set; }
        public string Address { get; set; }
        public string Country { get; set; }

    }
}
