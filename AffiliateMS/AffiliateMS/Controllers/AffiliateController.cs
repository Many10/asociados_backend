﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AffiliateMS.Domain.Entities;
using AffiliateMS.Models;
using AffiliateMS.Service.Command;
using AffiliateMS.Service.Query;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Mvc;

namespace AffiliateMS.Controllers
{
    [Produces("application/json")]
    [Route("v1/[controller]")]
    [ApiController]
    public class AffiliateController : ControllerBase
    {

        private readonly IMediator mediator;
        private readonly IMapper mapper;

        public AffiliateController(IMediator mediator, IMapper mapper)
        {
            this.mediator = mediator;
            this.mapper = mapper;
        }

        /// <summary>
        /// Action to get all the affiliates
        /// </summary>
        /// <returns>The list with all the affiliates</returns>
        /// <response code="200">Returned with the list</response>
        /// <response code="400">Returned if something went wrong during the process</response>
        [HttpGet]
        public async Task<IActionResult> Affiliate()
        {
            try
            {
                var affiliate = await mediator.Send(new GetAffiliateListQuery());
                return Ok(affiliate);
            } catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

        /// <summary>
        /// Action to update an existing affiliate
        /// </summary>
        /// <param name="updateAffiliate">Model to update an existing affiliate</param>
        /// <returns>Returns the updated affiliate</returns>
        /// <response code="200">Returned if the affiliate was updated</response>
        /// <response code="400">Returned if the model couldn't be parsed or the affiliate couldn't be found</response>
        [HttpPut]
        public async Task<ActionResult<Affiliate>> Affiliate([FromBody] AffiliateModel updateAffiliate)
        {
            try
            {
                var affiliate = await mediator.Send(new GetAffiliateByIdQuery
                {
                    Id = updateAffiliate.Id
                });

                if (affiliate == null)
                {
                    return BadRequest($"No customer found with the id {updateAffiliate.Id}");
                }

                return await mediator.Send(new UpdateAffiliateCommand
                {
                    Affiliate = mapper.Map(updateAffiliate, affiliate)
                });

            } catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
