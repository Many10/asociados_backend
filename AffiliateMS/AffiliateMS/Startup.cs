using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using AffiliateMS.DataAccess.Database;
using AffiliateMS.DataAccess.Repository;
using AffiliateMS.Domain.Entities;
using AffiliateMS.Messaging.Send.Options;
using AffiliateMS.Messaging.Send.Sender;
using AffiliateMS.Service.Command;
using AffiliateMS.Service.Query;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

namespace AffiliateMS
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            var connectionString = Configuration.GetConnectionString("SqlDbConnection");
            services.AddDbContext<AffiliateContext>(options => options.UseSqlServer(connectionString));

            services.Configure<RabbitMqConfiguration>(Configuration.GetSection("RabbitMq"));

            services.AddControllers();

            services.AddAutoMapper(typeof(Startup));

            services.AddCors(c =>
            {
                c.AddPolicy("CorsPolicy", options => {
                    options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
                });
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Affiliate Api",
                    Description = "Microservice for affiliates"
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            services.AddMediatR(Assembly.GetExecutingAssembly());
            services.AddTransient(typeof(IRepository<>), typeof(Repository<>));
            services.AddTransient<IAffiliateRepository, AffiliateRepository>();

            services.AddTransient<IAffiliateUpdateSender, AffiliateUpdateSender>();

            services.AddTransient<IRequestHandler<CreateAffiliateCommand, Affiliate>, CreateAffiliateCommandHandler>();
            services.AddTransient<IRequestHandler<UpdateAffiliateCommand, Affiliate>, UpdateAffiliateCommandHandler>();
            services.AddTransient<IRequestHandler<GetAffiliateListQuery, IEnumerable<Affiliate>>, GetAffiliateListQueryHandler>();
            services.AddTransient<IRequestHandler<GetAffiliateByIdQuery, Affiliate>, GetAffiliateByIdQueryHandler>();

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Affiliate API V1");
                c.RoutePrefix = string.Empty;
            });
            app.UseRouting();

            app.UseAuthorization();

            app.UseCors("CorsPolicy");

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
