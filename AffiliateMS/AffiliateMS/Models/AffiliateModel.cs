﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AffiliateMS.Models
{
    public class AffiliateModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Identification { get; set; }
        [Required]
        public string Address { get; set; }
        [Required]
        public string Country { get; set; }

    }
}
