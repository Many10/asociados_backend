﻿using AffiliateMS.Domain.Entities;
using AffiliateMS.Models;
using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AffiliateMS.AutoMapper
{
    public class MappingProfile : Profile
    {

        public MappingProfile()
        {
            CreateMap<AffiliateModel, Affiliate>();
        }

    }
}
