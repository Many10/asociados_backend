﻿using AffiliateMS.DataAccess.Database;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AffiliateMS.DataAccess.Repository
{
    public class Repository<T> : IRepository<T> where T: class, new()
    {
        protected readonly AffiliateContext affiliateContext;

        public Repository(AffiliateContext affiliateContext)
        {
            this.affiliateContext = affiliateContext;
        }

        public async Task<IEnumerable<T>> GetAll()
        {
            try
            {
                return affiliateContext.Set<T>();
            }
            catch (Exception ex)
            {
                throw new Exception($"Couldn't retrieve entities: {ex.Message}");
            }
        }

        public async Task<T> AddAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            try
            {
                await affiliateContext.AddAsync(entity);
                await affiliateContext.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be saved: {ex.Message}");
            }
        }

        public async Task<T> UpdateAsync(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException($"{nameof(AddAsync)} entity must not be null");
            }

            try
            {
                affiliateContext.Update(entity);
                await affiliateContext.SaveChangesAsync();

                return entity;
            }
            catch (Exception ex)
            {
                throw new Exception($"{nameof(entity)} could not be updated {ex.Message}");
            }
        }
    }
}
