﻿using AffiliateMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AffiliateMS.DataAccess.Repository
{
    public interface IAffiliateRepository : IRepository<Affiliate>
    {

        Task<Affiliate> GetAffiliateByIdAsync(int id, CancellationToken cancellationToken);

    }
}
