﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace AffiliateMS.DataAccess.Repository
{
    public interface IRepository<T> where T: class, new()
    {

        Task<IEnumerable<T>> GetAll();

        Task<T> AddAsync(T entity);

        Task<T> UpdateAsync(T entity);

    }
}
