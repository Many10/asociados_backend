﻿using System;
using System.Threading;
using System.Threading.Tasks;
using AffiliateMS.Domain.Entities;
using Microsoft.EntityFrameworkCore;
using AffiliateMS.DataAccess.Database;

namespace AffiliateMS.DataAccess.Repository
{
    public class AffiliateRepository : Repository<Affiliate>, IAffiliateRepository
    {
        public AffiliateRepository(AffiliateContext customerContext) : base(customerContext)
        {
        }

        public async Task<Affiliate> GetAffiliateByIdAsync(int id, CancellationToken cancellationToken)
        {
            return await affiliateContext.Affiliate.FirstOrDefaultAsync(x => x.Id == id, cancellationToken);
        }
    }
}
