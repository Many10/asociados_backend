﻿using AffiliateMS.Domain.Entities;
using Microsoft.EntityFrameworkCore;

namespace AffiliateMS.DataAccess.Database
{
    public class AffiliateContext : DbContext
    {

        public AffiliateContext(DbContextOptions<AffiliateContext> options)
            : base(options)
        {
        }

        public DbSet<Affiliate> Affiliate { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Affiliate>(entity =>
            {
                entity.Property(e => e.Id).IsRequired();

                entity.Property(e => e.Name).IsRequired();

                entity.Property(e => e.Identification).IsRequired();

                entity.Property(e => e.Address).IsRequired();

                entity.Property(e => e.Country).IsRequired();
            });
        }
    }
}
