﻿using AffiliateMS.DataAccess.Repository;
using AffiliateMS.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AffiliateMS.Service.Command
{
    public class CreateAffiliateCommandHandler : IRequestHandler<CreateAffiliateCommand, Affiliate>
    {
        private readonly IAffiliateRepository affiliateRepository;

        public CreateAffiliateCommandHandler(IAffiliateRepository affiliateRepository)
        {
            this.affiliateRepository = affiliateRepository;
        }

        public async Task<Affiliate> Handle(CreateAffiliateCommand request, CancellationToken cancellationToken)
        {
            return await affiliateRepository.AddAsync(request.Affiliate);
        }
    }
}
