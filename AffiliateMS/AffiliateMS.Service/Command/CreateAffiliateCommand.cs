﻿using AffiliateMS.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace AffiliateMS.Service.Command
{
    public class CreateAffiliateCommand : IRequest<Affiliate>
    {
        public Affiliate Affiliate { get; set; }

    }
}
