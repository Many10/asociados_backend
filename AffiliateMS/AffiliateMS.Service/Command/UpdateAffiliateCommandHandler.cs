﻿using AffiliateMS.DataAccess.Repository;
using AffiliateMS.Domain.Entities;
using AffiliateMS.Messaging.Send.Sender;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace AffiliateMS.Service.Command
{
    public class UpdateAffiliateCommandHandler : IRequestHandler<UpdateAffiliateCommand, Affiliate>
    {
        public readonly IAffiliateRepository affiliateRepository;
        public readonly IAffiliateUpdateSender updateAffiliateSender;

        public UpdateAffiliateCommandHandler(IAffiliateRepository affiliateRepository, IAffiliateUpdateSender updateAffiliateSender)
        {
            this.affiliateRepository = affiliateRepository;
            this.updateAffiliateSender = updateAffiliateSender;
        }

        public async Task<Affiliate> Handle(UpdateAffiliateCommand request, CancellationToken cancellationToken)
        {
            var affiliate = await affiliateRepository.UpdateAsync(request.Affiliate);

            updateAffiliateSender.SendAffiliate(affiliate);

            return affiliate;
        }
    }
}
