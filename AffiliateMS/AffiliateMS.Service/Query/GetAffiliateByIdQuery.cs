﻿using AffiliateMS.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace AffiliateMS.Service.Query
{
    public class GetAffiliateByIdQuery : IRequest<Affiliate>
    {

        public int Id { get; set; }
    }
}
