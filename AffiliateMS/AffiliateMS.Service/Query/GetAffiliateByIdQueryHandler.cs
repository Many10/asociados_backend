﻿using AffiliateMS.DataAccess.Repository;
using AffiliateMS.Domain.Entities;
using MediatR;
using System.Threading;
using System.Threading.Tasks;

namespace AffiliateMS.Service.Query
{
    public class GetAffiliateByIdQueryHandler : IRequestHandler<GetAffiliateByIdQuery, Affiliate>
    {
        private readonly IAffiliateRepository affiliateRepository;

        public GetAffiliateByIdQueryHandler(IAffiliateRepository affiliateRepository)
        {
            this.affiliateRepository = affiliateRepository;
        }

        public async Task<Affiliate> Handle(GetAffiliateByIdQuery request, CancellationToken cancellationToken)
        {
            return await affiliateRepository.GetAffiliateByIdAsync(request.Id, cancellationToken);
        }
    }
}
