﻿using AffiliateMS.DataAccess.Repository;
using AffiliateMS.Domain.Entities;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AffiliateMS.Service.Query
{
    public class GetAffiliateListQueryHandler : IRequestHandler<GetAffiliateListQuery, IEnumerable<Affiliate>>
    {
        private readonly IAffiliateRepository affiliateRepository;

        public GetAffiliateListQueryHandler(IAffiliateRepository affiliateRepository)
        {
            this.affiliateRepository = affiliateRepository;
        }


        public async Task<IEnumerable<Affiliate>> Handle(GetAffiliateListQuery request, CancellationToken cancellationToken)
        {
            return await affiliateRepository.GetAll();
        }
    }
}
