using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using ProductMS.DataAccess.Database;
using ProductMS.DataAccess.Repository;
using ProductMS.Domain.Entities;
using ProductMS.Messaging.Receive.Options;
using ProductMS.Messaging.Receive.Receiver;
using ProductMS.Service.Command;
using ProductMS.Service.Query;
using ProductMS.Service.Services;

namespace ProductMS
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddOptions();

            services.Configure<RabbitMqConfiguration>(Configuration.GetSection("RabbitMq"));

            var connectionString = Configuration.GetConnectionString("SqlDbConnection");
            services.AddDbContext<ProductContext>(options => options.UseSqlServer(connectionString));

            services.AddControllers();

            services.AddAutoMapper(typeof(Startup));

            services.AddCors(c =>
            {
                c.AddPolicy("CorsPolicy", options => {
                    options.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
                });
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Product Api",
                    Description = "Microservice for products"
                });

                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            services.AddMediatR(Assembly.GetExecutingAssembly(), typeof(IAffiliateUpdatingService).Assembly);

            services.AddScoped(typeof(IRepository<>), typeof(Repository<>));
            services.AddScoped<IProductRepository, ProductRepository>();

            services.AddTransient<IRequestHandler<UpdateProductCommand>, UpdateProductCommandHandler>();
            services.AddTransient<IRequestHandler<GetProductListByAffiliateIdQuery, List<Product>>, GetProductListByAffiliateIdQueryHandler>();
            services.AddTransient<IAffiliateUpdatingService, AffiliateUpdatingService>();

            services.AddHostedService<AffiliateUpdatingReceiver>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Product API V1");
                c.RoutePrefix = string.Empty;
            });

            app.UseRouting();

            app.UseCors("CorsPolicy");

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
