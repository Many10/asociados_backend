﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ProductMS.Models
{
    public class ProductModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public string AccountNumber { get; set; }
        [Required]
        public string TypeProduct { get; set; }
        [Required]
        public int AffiliateId { get; set; }
        [Required]
        public string AffiliateName { get; set; }
        [Required]
        public string AffiliateIdentification { get; set; }
        [Required]
        public bool Active { get; set; }
        [Required]
        public double Balance { get; set; }

    }
}
