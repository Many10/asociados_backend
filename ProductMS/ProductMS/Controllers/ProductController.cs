﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using ProductMS.Service.Query;

namespace ProductMS.Controllers
{
    [Produces("application/json")]
    [Route("v1/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {

        private readonly IMediator mediator;
        private readonly IMapper mapper;

        public ProductController(IMediator mediator, IMapper mapper)
        {
            this.mediator = mediator;
            this.mapper = mapper;
        }

        /// <summary>
        /// Action to get all the affiliates
        /// </summary>
        /// <returns>The list with all the affiliates</returns>
        /// <response code="200">Returned with the list</response>
        /// <response code="400">Returned if something went wrong during the process</response>
        [HttpGet("{affiliateId}")]
        public async Task<IActionResult> Product(int affiliateId)
        {
            try
            {
                var affiliate = await mediator.Send(new GetProductListByAffiliateIdQuery
                {
                    AffiliateId = affiliateId
                });
                return Ok(affiliate);
            }
            catch (Exception e)
            {
                return BadRequest(e.Message);
            }
        }

    }
}
 