﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductMS.Domain.Entities
{
    public class Product
    {

        public int Id { get; set; }
        public string AccountNumber { get; set; }
        public string TypeProduct { get; set; }
        public int AffiliateId { get; set; }
        public string AffiliateName { get; set; }
        public string AffiliateIdentification { get; set; }
        public bool Active { get; set; }
        public double Balance { get; set; }

    }
}
