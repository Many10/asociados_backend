﻿using MediatR;
using ProductMS.Service.Command;
using ProductMS.Service.Model;
using ProductMS.Service.Query;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductMS.Service.Services
{
    public class AffiliateUpdatingService : IAffiliateUpdatingService
    {
        public readonly IMediator mediator;

        public AffiliateUpdatingService(IMediator mediator)
        {
            this.mediator = mediator;
        }

        public async void UpdateAffiliateInProduct(AffiliateUpdateModel affiliateUpdateModel)
        {
            try
            {
                var productsOfAffiliate = await mediator.Send(new GetProductListByAffiliateIdQuery
                {
                    AffiliateId = affiliateUpdateModel.Id
                });

                if (productsOfAffiliate.Count != 0)
                {
                    productsOfAffiliate.ForEach(x =>
                    {
                        x.AffiliateName = affiliateUpdateModel.Name;
                        x.AffiliateId = affiliateUpdateModel.Id;
                        x.AffiliateIdentification = affiliateUpdateModel.Identification;
                    });
                }

                await mediator.Send(new UpdateProductCommand
                {
                    Products = productsOfAffiliate
                });
            } catch (Exception e)
            {
                throw new Exception($"Something went wrong: {e.Message}");
            }
        }
    }
}
