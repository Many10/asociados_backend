﻿using ProductMS.Service.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductMS.Service.Services
{
    public interface IAffiliateUpdatingService
    {

        void UpdateAffiliateInProduct(AffiliateUpdateModel affiliateUpdateModel);

    }
}
