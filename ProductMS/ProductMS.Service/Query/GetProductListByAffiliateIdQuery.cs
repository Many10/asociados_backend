﻿using MediatR;
using ProductMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductMS.Service.Query
{
    public class GetProductListByAffiliateIdQuery : IRequest<List<Product>>
    {

        public int AffiliateId { get; set; }

    }
}
