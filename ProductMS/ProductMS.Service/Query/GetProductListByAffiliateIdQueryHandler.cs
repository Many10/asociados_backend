﻿using MediatR;
using ProductMS.DataAccess.Repository;
using ProductMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProductMS.Service.Query
{
    public class GetProductListByAffiliateIdQueryHandler : IRequestHandler<GetProductListByAffiliateIdQuery, List<Product>>
    {
        private readonly IProductRepository productRepository;

        public GetProductListByAffiliateIdQueryHandler(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public async Task<List<Product>> Handle(GetProductListByAffiliateIdQuery request, CancellationToken cancellationToken)
        {
            return await productRepository.GetProductListByIdAffiliate(request.AffiliateId, cancellationToken);
        }
    }
}
