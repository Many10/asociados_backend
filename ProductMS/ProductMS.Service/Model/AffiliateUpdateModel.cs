﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProductMS.Service.Model
{
    public class AffiliateUpdateModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Identification { get; set; }

    }
}
