﻿using MediatR;
using ProductMS.DataAccess.Repository;
using ProductMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProductMS.Service.Command
{
    public class UpdateProductCommandHandler : IRequestHandler<UpdateProductCommand>
    {
        public readonly IProductRepository productRepository;

        public UpdateProductCommandHandler(IProductRepository productRepository)
        {
            this.productRepository = productRepository;
        }

        public async Task<Unit> Handle(UpdateProductCommand request, CancellationToken cancellationToken)
        {
            await productRepository.UpdateRangeAsync(request.Products);
            return Unit.Value;
        }
    }
}
