﻿using MediatR;
using ProductMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductMS.Service.Command
{
    public class UpdateProductCommand : IRequest
    {

        public List<Product> Products { get; set; }

    }
}
