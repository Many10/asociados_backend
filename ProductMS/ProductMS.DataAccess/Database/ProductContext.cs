﻿using Microsoft.EntityFrameworkCore;
using ProductMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace ProductMS.DataAccess.Database
{
    public class ProductContext : DbContext
    {

        public ProductContext()
        {}

        public ProductContext(DbContextOptions<ProductContext> options)
            : base(options)
        {}

        public DbSet<Product> Product { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079");

            modelBuilder.Entity<Product>(entity =>
            {
                entity.Property(e => e.Id).IsRequired();

                entity.Property(e => e.AccountNumber).IsRequired();

                entity.Property(e => e.AffiliateName).IsRequired();

                entity.Property(e => e.AffiliateIdentification).IsRequired();

                entity.Property(e => e.AffiliateId).IsRequired();

                entity.Property(e => e.Active).IsRequired();

                entity.Property(e => e.Balance).IsRequired();
            });
        }

    }
}
