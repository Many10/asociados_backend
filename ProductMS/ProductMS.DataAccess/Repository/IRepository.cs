﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace ProductMS.DataAccess.Repository
{
    public interface IRepository<T> where T: class, new()
    {
        Task<T> AddAsync(T entity);

        Task<T> UpdateAsync(T entity);

        Task UpdateRangeAsync(List<T> entities);
    }
}
