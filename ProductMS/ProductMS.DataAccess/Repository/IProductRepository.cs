﻿using ProductMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProductMS.DataAccess.Repository
{
    public interface IProductRepository: IRepository<Product>
    {

        Task<List<Product>> GetProductListByIdAffiliate(int idAffiliate, CancellationToken cancellationToken);

    }
}
