﻿using Microsoft.EntityFrameworkCore;
using ProductMS.DataAccess.Database;
using ProductMS.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ProductMS.DataAccess.Repository
{
    public class ProductRepository : Repository<Product>, IProductRepository
    {
        public ProductRepository(ProductContext customerContext) : base(customerContext)
        {
        }

        public async Task<List<Product>> GetProductListByIdAffiliate(int affiliateId, CancellationToken cancellationToken)
        {
            return await productContext.Product.Where(x => x.AffiliateId == affiliateId).ToListAsync();
        }
    }
}
